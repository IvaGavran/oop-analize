using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace analiza7
{
	public partial class Krizickruzic : Form
	{
		bool oznaka = true;
		int brojac = 0;
		static String prvi, drugi;
		public Krizickruzic()
		{
			InitializeComponent();
		}
		public static void SetPlayerNames(String a, String b)
		{
			prvi = a;
			drugi = b;
		}
		private void btn_Click(object sender, EventArgs e)
		{
			Button tempBtn = (Button)sender;
			if (oznaka)
			{
				tempBtn.Text = "X";
			}
			else
			{
				tempBtn.Text = "O";
			}
			oznaka = !oznaka;
			tempBtn.Enabled = false;
			brojac++;
			TkoPobjeduje();
		}
		public void TkoPobjeduje()
		{
			bool Pobjednik = false;
			if ((btn1.Text == btn2.Text) && (btn2.Text == btn3.Text) && (!

				btn1.Enabled)) {
				Pobjednik = true;
			}

			else if ((btn4.Text == btn5.Text) && (btn5.Text == btn6.Text) &&

				(!btn4.Enabled)) {
				Pobjednik = true;
			}

			else if ((btn7.Text == btn8.Text) && (btn8.Text == btn9.Text) &&

				(!btn7.Enabled)) {
				Pobjednik = true;
			}

			else if ((btn1.Text == btn4.Text) && (btn4.Text == btn7.Text) &&

				(!btn1.Enabled)) {
				Pobjednik = true;
			}

			else if ((btn2.Text == btn5.Text) && (btn5.Text == btn8.Text) &&

				(!btn2.Enabled)) {
				Pobjednik = true;
			}

			else if ((btn3.Text == btn6.Text) && (btn6.Text == btn9.Text) &&

				(!btn3.Enabled)) {
				Pobjednik = true;
			}

			else if ((btn1.Text == btn5.Text) && (btn5.Text == btn9.Text) &&

				(!btn1.Enabled)) {
				Pobjednik = true;
			}

			else if ((btn3.Text == btn5.Text) && (btn5.Text == btn7.Text) &&

				(!btn3.Enabled)) {
				Pobjednik = true;
			}

			int brojac2 = 0;
			int brojac3 = 0;
			int brojac4 = 0;
			if (Pobjednik)
			{
				Disable();
				String pobjeda = "";
				if (oznaka)
				{
					pobjeda = prvi;
					brojac2++;
				}
				else
				{
					pobjeda = drugi;
					brojac3++;
				}
				MessageBox.Show("Pobjednik je:" + pobjeda);
			}
			else if (brojac == 9)
			{
				brojac4++;
				MessageBox.Show("Nerijeseno.");
			}
			lbl_prviDobio.Text = brojac2.ToString();
			lbl_Draws.Text = brojac4.ToString();
			lbl_drugiDobio.Text = brojac3.ToString();
		}
		public void Disable()
		{
			try
			{
				foreach(Control c in Controls)
				{
					Button b = (Button)c;
					b.Enabled = false;
				}
			}
			catch { }

		}

		private void NewGameToolStripMenuItem_Click(object sender, EventArgs e)
		{
			oznaka = true;
			brojac = 0;
		}
	}
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace analiza7
{
	public partial class Imena : Form
	{
		public Imena()
		{
			InitializeComponent();
		}
		private void btn1_Click(object sender, EventArgs e)
		{
			Krizickruzic.SetImena(tB_prvi.Text, tB_drugi.Text);
			this.Close();
		}
	}
}