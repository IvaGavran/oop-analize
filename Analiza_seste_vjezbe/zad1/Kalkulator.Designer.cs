namespace analizaprvi
{
    partial class Kalkulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_zbroji = new System.Windows.Forms.Button();
            this.btn_oduzmi = new System.Windows.Forms.Button();
            this.btn_pomnozi = new System.Windows.Forms.Button();
            this.btn_podijeli = new System.Windows.Forms.Button();
            this.btn_sin = new System.Windows.Forms.Button();
            this.btn_log = new System.Windows.Forms.Button();
            this.btn_cos = new System.Windows.Forms.Button();
            this.btn_exp = new System.Windows.Forms.Button();
            this.btn_sqrt = new System.Windows.Forms.Button();
            this.tB_operand1 = new System.Windows.Forms.TextBox();
            this.tB_operand2 = new System.Windows.Forms.TextBox();
            this.tB_rezultat = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btn_zbroji
            // 
            this.btn_zbroji.Location = new System.Drawing.Point(349, 82);
            this.btn_zbroji.Name = "btn_zbroji";
            this.btn_zbroji.Size = new System.Drawing.Size(75, 23);
            this.btn_zbroji.TabIndex = 0;
            this.btn_zbroji.Text = "Zbroji";
            this.btn_zbroji.UseVisualStyleBackColor = true;
            this.btn_zbroji.Click += new System.EventHandler(this.Btn_zbroji_Click);
            // 
            // btn_oduzmi
            // 
            this.btn_oduzmi.Location = new System.Drawing.Point(349, 121);
            this.btn_oduzmi.Name = "btn_oduzmi";
            this.btn_oduzmi.Size = new System.Drawing.Size(75, 23);
            this.btn_oduzmi.TabIndex = 1;
            this.btn_oduzmi.Text = "Oduzmi";
            this.btn_oduzmi.UseVisualStyleBackColor = true;
            this.btn_oduzmi.Click += new System.EventHandler(this.Btn_oduzmi_Click);
            // 
            // btn_pomnozi
            // 
            this.btn_pomnozi.Location = new System.Drawing.Point(349, 159);
            this.btn_pomnozi.Name = "btn_pomnozi";
            this.btn_pomnozi.Size = new System.Drawing.Size(75, 23);
            this.btn_pomnozi.TabIndex = 2;
            this.btn_pomnozi.Text = "Pomnoži";
            this.btn_pomnozi.UseVisualStyleBackColor = true;
            this.btn_pomnozi.Click += new System.EventHandler(this.Btn_pomnozi_Click);
            // 
            // btn_podijeli
            // 
            this.btn_podijeli.Location = new System.Drawing.Point(349, 203);
            this.btn_podijeli.Name = "btn_podijeli";
            this.btn_podijeli.Size = new System.Drawing.Size(75, 23);
            this.btn_podijeli.TabIndex = 3;
            this.btn_podijeli.Text = "Podijeli";
            this.btn_podijeli.UseVisualStyleBackColor = true;
            this.btn_podijeli.Click += new System.EventHandler(this.Btn_podijeli_Click);
            // 
            // btn_sin
            // 
            this.btn_sin.Location = new System.Drawing.Point(464, 72);
            this.btn_sin.Name = "btn_sin";
            this.btn_sin.Size = new System.Drawing.Size(75, 23);
            this.btn_sin.TabIndex = 4;
            this.btn_sin.Text = "sin";
            this.btn_sin.UseVisualStyleBackColor = true;
            this.btn_sin.Click += new System.EventHandler(this.Btn_sin_Click);
            // 
            // btn_log
            // 
            this.btn_log.Location = new System.Drawing.Point(464, 159);
            this.btn_log.Name = "btn_log";
            this.btn_log.Size = new System.Drawing.Size(75, 23);
            this.btn_log.TabIndex = 5;
            this.btn_log.Text = "log";
            this.btn_log.UseVisualStyleBackColor = true;
            this.btn_log.Click += new System.EventHandler(this.Btn_log_Click);
            // 
            // btn_cos
            // 
            this.btn_cos.Location = new System.Drawing.Point(464, 121);
            this.btn_cos.Name = "btn_cos";
            this.btn_cos.Size = new System.Drawing.Size(75, 23);
            this.btn_cos.TabIndex = 6;
            this.btn_cos.Text = "cos";
            this.btn_cos.UseVisualStyleBackColor = true;
            this.btn_cos.Click += new System.EventHandler(this.Btn_cos_Click);
            // 
            // btn_exp
            // 
            this.btn_exp.Location = new System.Drawing.Point(464, 243);
            this.btn_exp.Name = "btn_exp";
            this.btn_exp.Size = new System.Drawing.Size(75, 23);
            this.btn_exp.TabIndex = 7;
            this.btn_exp.Text = "exp";
            this.btn_exp.UseVisualStyleBackColor = true;
            this.btn_exp.Click += new System.EventHandler(this.Btn_exp_Click);
            // 
            // btn_sqrt
            // 
            this.btn_sqrt.Location = new System.Drawing.Point(464, 203);
            this.btn_sqrt.Name = "btn_sqrt";
            this.btn_sqrt.Size = new System.Drawing.Size(75, 23);
            this.btn_sqrt.TabIndex = 8;
            this.btn_sqrt.Text = "sqrt";
            this.btn_sqrt.UseVisualStyleBackColor = true;
            this.btn_sqrt.Click += new System.EventHandler(this.Btn_sqrt_Click);
            // 
            // tB_operand1
            // 
            this.tB_operand1.Location = new System.Drawing.Point(132, 98);
            this.tB_operand1.Name = "tB_operand1";
            this.tB_operand1.Size = new System.Drawing.Size(100, 20);
            this.tB_operand1.TabIndex = 9;
            // 
            // tB_operand2
            // 
            this.tB_operand2.Location = new System.Drawing.Point(132, 159);
            this.tB_operand2.Name = "tB_operand2";
            this.tB_operand2.Size = new System.Drawing.Size(100, 20);
            this.tB_operand2.TabIndex = 10;
            // 
            // tB_rezultat
            // 
            this.tB_rezultat.Location = new System.Drawing.Point(633, 147);
            this.tB_rezultat.Name = "tB_rezultat";
            this.tB_rezultat.Size = new System.Drawing.Size(100, 20);
            this.tB_rezultat.TabIndex = 11;
            // 
            // Kalkulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tB_rezultat);
            this.Controls.Add(this.tB_operand2);
            this.Controls.Add(this.tB_operand1);
            this.Controls.Add(this.btn_sqrt);
            this.Controls.Add(this.btn_exp);
            this.Controls.Add(this.btn_cos);
            this.Controls.Add(this.btn_log);
            this.Controls.Add(this.btn_sin);
            this.Controls.Add(this.btn_podijeli);
            this.Controls.Add(this.btn_pomnozi);
            this.Controls.Add(this.btn_oduzmi);
            this.Controls.Add(this.btn_zbroji);
            this.Name = "Kalkulator";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_zbroji;
        private System.Windows.Forms.Button btn_oduzmi;
        private System.Windows.Forms.Button btn_pomnozi;
        private System.Windows.Forms.Button btn_podijeli;
        private System.Windows.Forms.Button btn_sin;
        private System.Windows.Forms.Button btn_log;
        private System.Windows.Forms.Button btn_cos;
        private System.Windows.Forms.Button btn_exp;
        private System.Windows.Forms.Button btn_sqrt;
        private System.Windows.Forms.TextBox tB_operand1;
        private System.Windows.Forms.TextBox tB_operand2;
        private System.Windows.Forms.TextBox tB_rezultat;
    }
}