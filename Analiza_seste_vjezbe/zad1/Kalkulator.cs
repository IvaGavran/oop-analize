using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace analizaprvi
{
    public partial class Kalkulator : Form
    {
        double x, y, rez;

        public Kalkulator()
        {
            InitializeComponent();
        }

        private void Btn_zbroji_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB_operand1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            if (!double.TryParse(tB_operand2.Text, out y))
            {
                MessageBox.Show("Krivi unos operanda 2!", "Pogreška");
            }
            rez = x + y;
            tB_rezultat.Text = rez.ToString();
            tB_rezultat.Show();
        }

        private void Btn_oduzmi_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB_operand1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            if (!double.TryParse(tB_operand2.Text, out y))
            {
                MessageBox.Show("Krivi unos operanda 2!", "Pogreška");
            }
            rez = x - y;
            tB_rezultat.Text = rez.ToString();
            tB_rezultat.Show();
        }

        private void Btn_pomnozi_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB_operand1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            if (!double.TryParse(tB_operand2.Text, out y))
            {
                MessageBox.Show("Krivi unos operanda 2!", "Pogreška");
            }
            rez = x * y;
            tB_rezultat.Text = rez.ToString();
            tB_rezultat.Show();
        }

       
        private void Btn_podijeli_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB_operand1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            if (!double.TryParse(tB_operand2.Text, out y) || y == 0)
            {
                MessageBox.Show("Krivi unos operanda 2!", "Pogreška");
            }
            rez = x / y;
            tB_rezultat.Text = rez.ToString();
            tB_rezultat.Show();
        }

        private void Btn_sin_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB_operand1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            rez = Math.Sin(x);
            tB_rezultat.Text = rez.ToString();
            tB_rezultat.Show();
        }

        private void Btn_cos_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB_operand1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            rez = Math.Cos(x);
            tB_rezultat.Text = rez.ToString();
            tB_rezultat.Show();
        }

        private void Btn_log_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB_operand1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            rez = Math.Log(x);
            tB_rezultat.Text = rez.ToString();
            tB_rezultat.Show();
        }

        private void Btn_sqrt_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB_operand1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            rez = Math.Sqrt(x);
            tB_rezultat.Text = rez.ToString();
            tB_rezultat.Show();
        }

        private void Btn_exp_Click(object sender, EventArgs e)
        {
            if (!double.TryParse(tB_operand1.Text, out x))
            {
                MessageBox.Show("Krivi unos operanda 1!", "Pogreška");
            }
            rez = Math.Exp(x);
            tB_rezultat.Text = rez.ToString();
            tB_rezultat.Show();
        }

    }
}