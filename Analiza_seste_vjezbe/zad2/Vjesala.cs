using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vjesala
{
    public partial class Form1 : Form
    {
        private string path = "C:\\Users\\Iva\\data.txt";

        private List<string> pojmovi = new List<string> {};

        private string pojam = "A";

        private char slovo = 'A';

        private Random random = new Random();

        private int brojPokusajaNum = 0;

        private static bool isLetter(char c)
        {
            return ('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z');
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void novaIgra()
        {
            pojam = pojmovi.ElementAt(random.Next(0, pojmovi.Count - 1));
            brojPokusajaNum = pojam.Length;
            lb_brojPokusajaNum.Text = (brojPokusajaNum).ToString();
            lb_pojam.Text = new String('?', brojPokusajaNum);
        }

        private void btn_novaIgra_Click(object sender, EventArgs e)
        {
            this.novaIgra();
        }

        private void btn_odaberiSlovo_Click(object sender, EventArgs e)
        {
            if (!char.TryParse(tB_slovo.Text, out slovo)) {
                MessageBox.Show("Nije unesen znak!");
            } else if (isLetter(slovo))
            {
                // prebaci unos u veliko slovo
                slovo = ((slovo.ToString()).ToUpper())[0];
                if (pojam.Contains(slovo))
                {
                    char[] newText = lb_pojam.Text.ToCharArray();
                    for (int i = 0; i < pojam.Length; i++)
                    {
                        if (slovo.Equals(pojam[i]))
                        {
                            newText[i] = slovo;
                        }
                    }
                    lb_pojam.Text = new String(newText);

                    if (!lb_pojam.Text.Contains('?'))
                    {
                        MessageBox.Show("Pobjedili ste!");
                        this.novaIgra();
                    }
                } else
                {
                    brojPokusajaNum--;
                    lb_brojPokusajaNum.Text = (brojPokusajaNum).ToString();

                    if (0 == brojPokusajaNum)
                    {
                        lb_pojam.Text = pojam;
                        MessageBox.Show("Izgubili ste!");
                        this.novaIgra();
                    }
                }
            } else
            {
                MessageBox.Show("Uneseni znak nije slovo!");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // ucitaj pojmove iz datoteke
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null) // čitanje svih linija iz dat.
                {
                    foreach (char c in line)
                    {
                        if (!isLetter(c))
                        {
                            continue; // preskoci pojam ako sadrzi druge znakove osim slova
                        }
                    }
                    pojmovi.Add(line);
                }
            }
            if (!pojmovi.Any())
            {
                MessageBox.Show("Datoteka @ " + path + " je ili prazna ili ne sadrzi validne pojmove." +
                                "\nIzlazim iz aplikacije.   ");
                Application.Exit();
            }
            // prebaci sve u velika slova
            for (int i = 0; i < pojmovi.Count - 1; i++)
            {
                pojmovi[i] = pojmovi[i].ToUpper();
            }
        }
    }
}