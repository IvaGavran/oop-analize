namespace Vjesala
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lb_brojPokusaja = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.lb_brojPokusajaNum = new System.Windows.Forms.Label();
            this.btn_novaIgra = new System.Windows.Forms.Button();
            this.btn_odaberiSlovo = new System.Windows.Forms.Button();
            this.tB_slovo = new System.Windows.Forms.TextBox();
            this.lb_pojam = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lb_brojPokusaja
            // 
            this.lb_brojPokusaja.AutoSize = true;
            this.lb_brojPokusaja.Location = new System.Drawing.Point(64, 149);
            this.lb_brojPokusaja.Name = "lb_brojPokusaja";
            this.lb_brojPokusaja.Size = new System.Drawing.Size(74, 13);
            this.lb_brojPokusaja.TabIndex = 0;
            this.lb_brojPokusaja.Text = "Broj pokusaja:";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // lb_brojPokusajaNum
            // 
            this.lb_brojPokusajaNum.AutoSize = true;
            this.lb_brojPokusajaNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_brojPokusajaNum.Location = new System.Drawing.Point(157, 149);
            this.lb_brojPokusajaNum.Name = "lb_brojPokusajaNum";
            this.lb_brojPokusajaNum.Size = new System.Drawing.Size(14, 13);
            this.lb_brojPokusajaNum.TabIndex = 3;
            this.lb_brojPokusajaNum.Text = "0";
            this.lb_brojPokusajaNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btn_novaIgra
            // 
            this.btn_novaIgra.Location = new System.Drawing.Point(73, 26);
            this.btn_novaIgra.Name = "btn_novaIgra";
            this.btn_novaIgra.Size = new System.Drawing.Size(75, 23);
            this.btn_novaIgra.TabIndex = 4;
            this.btn_novaIgra.Text = "Nova igra!";
            this.btn_novaIgra.UseVisualStyleBackColor = true;
            this.btn_novaIgra.Click += new System.EventHandler(this.btn_novaIgra_Click);
            // 
            // btn_odaberiSlovo
            // 
            this.btn_odaberiSlovo.Location = new System.Drawing.Point(57, 113);
            this.btn_odaberiSlovo.Name = "btn_odaberiSlovo";
            this.btn_odaberiSlovo.Size = new System.Drawing.Size(91, 23);
            this.btn_odaberiSlovo.TabIndex = 5;
            this.btn_odaberiSlovo.Text = "Odaberi slovo!";
            this.btn_odaberiSlovo.UseVisualStyleBackColor = true;
            this.btn_odaberiSlovo.Click += new System.EventHandler(this.btn_odaberiSlovo_Click);
            // 
            // tB_slovo
            // 
            this.tB_slovo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tB_slovo.Location = new System.Drawing.Point(154, 115);
            this.tB_slovo.Name = "tB_slovo";
            this.tB_slovo.Size = new System.Drawing.Size(20, 20);
            this.tB_slovo.TabIndex = 6;
            this.tB_slovo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lb_pojam
            // 
            this.lb_pojam.AutoSize = true;
            this.lb_pojam.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_pojam.Location = new System.Drawing.Point(65, 68);
            this.lb_pojam.Name = "lb_pojam";
            this.lb_pojam.Size = new System.Drawing.Size(100, 29);
            this.lb_pojam.TabIndex = 7;
            this.lb_pojam.Text = "POJAM";
            this.lb_pojam.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(230, 187);
            this.Controls.Add(this.lb_pojam);
            this.Controls.Add(this.tB_slovo);
            this.Controls.Add(this.btn_odaberiSlovo);
            this.Controls.Add(this.btn_novaIgra);
            this.Controls.Add(this.lb_brojPokusajaNum);
            this.Controls.Add(this.lb_brojPokusaja);
            this.Name = "Form1";
            this.Text = "Vjesala";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lb_brojPokusaja;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.Label lb_brojPokusajaNum;
        private System.Windows.Forms.Button btn_novaIgra;
        private System.Windows.Forms.Button btn_odaberiSlovo;
        private System.Windows.Forms.TextBox tB_slovo;
        private System.Windows.Forms.Label lb_pojam;
    }
}